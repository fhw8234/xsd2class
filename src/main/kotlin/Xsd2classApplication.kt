import org.w3c.dom.Node
import org.w3c.dom.NodeList
import java.io.File
import javax.xml.parsers.DocumentBuilderFactory


class Xsd2classApplication

fun main() {
    val file = File("pacs.008.001.10.xsd")
    val dbf = DocumentBuilderFactory.newInstance()
    val db = dbf.newDocumentBuilder()
    val doc = db.parse(file)
    doc.normalizeDocument()

    val simpleTypeNodes = doc.getElementsByTagName("xs:simpleType")
    val simpleTypeMap = genSimpleTypeMap(simpleTypeNodes)

    val simpleTypeList = simpleTypeMap.values.filter { it.type == "enum" }.map { it.toContent() }

    val complexTypeNodes = doc.getElementsByTagName("xs:complexType")
    val complexTypeList = loadComplexType(complexTypeNodes, simpleTypeMap)

    File("Document.kt").printWriter().use { out ->
        importList.forEach {
            out.println(it)
        }
        simpleTypeList.forEach { out.println(it) }
        complexTypeList.forEach { out.println(it) }
    }

    importList.forEach { println(it) }
    simpleTypeList.forEach { println(it) }
    complexTypeList.forEach { println(it) }

}

val importList = listOf("import java.math.BigDecimal",
        "import java.time.LocalDate",
        "import java.time.LocalDateTime",
        "import java.time.LocalTime",
        "import javax.validation.constraints.Pattern",
        "import javax.validation.constraints.Size", "import com.fasterxml.jackson.annotation.JsonProperty", "")

private fun genSimpleTypeMap(simpleTypeNodes: NodeList): MutableMap<String, SimpleType> {
    val simpleTypeMap = mutableMapOf<String, SimpleType>()
    for (i in 0 until simpleTypeNodes.length) {
        val enumElements: MutableList<String> = mutableListOf()
        val node = simpleTypeNodes.item(i)
        val name = node.attributes.getNamedItem("name").nodeValue
        val childNodes = node.childNodes
        val validationList = mutableListOf<String>()
        var simpleTypeType = "String"
        for (j in 0 until childNodes.length) {
            val childNode = childNodes.item(j)
            if (childNode.nodeName != "xs:restriction") continue

            val restrictionItems = childNode.childNodes
            val baseType = childNode.attributes.getNamedItem("base").nodeValue

            simpleTypeType = when (baseType) {
                "xs:decimal" -> "BigDecimal"
                "xs:boolean" -> "Boolean"
                "xs:date" -> "LocalDate"
                "xs:dateTime" -> "LocalDateTime"
                "xs:time" -> "LocalTime"
                else -> "String"
            }

            var pattern = ""
            val sizeList = mutableListOf<String>()

            for (k in 0 until restrictionItems.length) {
                val restrictionNode = restrictionItems.item(k)
                if (restrictionNode.nodeType == Node.TEXT_NODE) continue
                if (simpleTypeType == "String" && restrictionNode.nodeName == "xs:enumeration") {
                    simpleTypeType = "enum"
                }

                val restrictionNodeValue = restrictionNode.attributes.getNamedItem("value").nodeValue
                when (restrictionNode.nodeName) {
                    "xs:enumeration" -> {
                        if (simpleTypeType == "String") {
                            simpleTypeType = "enum"
                        }
                        enumElements.add(restrictionNodeValue)
                    }
                    "xs:pattern" -> pattern = restrictionNodeValue
                    "xs:minLength" -> sizeList.add("min = $restrictionNodeValue")
                    "xs:maxLength" -> sizeList.add("max = $restrictionNodeValue")
                    else -> {
                    }
                }
            }
            if (pattern != "") {
                validationList.add("@field:Pattern(regexp = \"$pattern\")")
            }
            if (sizeList.isNotEmpty()) {
                validationList.add("@field:Size(${sizeList.joinToString(separator = ", ")})")
            }
        }


        simpleTypeMap[name] = SimpleType(name, simpleTypeType, validationList, enumElements)
    }
    return simpleTypeMap
}

fun loadComplexType(complexTypeNodes: NodeList, simpleTypeMap: Map<String, SimpleType>): List<String> {
    val result = mutableListOf<String>()
    for (i in 0 until complexTypeNodes.length) {
        val list = mutableListOf<String>()
        val childNodes = complexTypeNodes.item(i).childNodes
        list.add("data class ${complexTypeNodes.item(i).attributes.getNamedItem("name").nodeValue} (")
        for (j in 0 until childNodes.length) {
            val childNode = childNodes.item(j)
            if (childNode.nodeName != "xs:sequence" && childNode.nodeName != "xs:choice") continue
            val sequenceList = childNode.childNodes
            for (k in 0 until sequenceList.length) {
                val seq = sequenceList.item(k)
                if (seq.nodeName != "xs:element") continue

                val isList = (seq.attributes.getNamedItem("maxOccurs")?.nodeValue ?: "1") != "1"

                val originalFieldType = seq.attributes.getNamedItem("type").nodeValue

                val tempType = if (simpleTypeMap[originalFieldType]?.type == null || simpleTypeMap[originalFieldType]?.type == "enum") {
                                    originalFieldType
                               } else {
                                    simpleTypeMap[originalFieldType]?.type
                               }

                val optional = if (seq.attributes.getNamedItem("minOccurs")?.nodeValue == "0" || childNode.nodeName != "xs:choice") "?" else ""
                val type = if (isList) "List<$tempType>$optional" else "$tempType$optional"


                val tempValidation = simpleTypeMap[originalFieldType]?.validationList?.joinToString(separator = " ")
                val validation = tempValidation ?: ""

                val jsonFieldName = seq.attributes.getNamedItem("name").nodeValue
                val field = jsonFieldName.replaceFirstChar { it.lowercaseChar() }

                list.add("    $validation @JsonProperty(value = \"$jsonFieldName\") val $field: $type,")
            }
        }
        list.add(")")
        result.add(list.joinToString(separator = "\n"))
    }
    return result
}

data class ComplexType(val name: String)

data class SimpleType(val name: String,
                      val type: String,
                      val validationList: List<String>,
                      val enumValues: List<String>) {
    fun toContent(): String {
        return when(type) {
            "enum" -> """
                enum class $name(val value: String) {
                    ${toEnum()}
                }    
            """.trimIndent()
            else -> validationList.joinToString(separator = " ")
        }
    }

    private fun toEnum(): String {
        return enumValues.joinToString(separator = ",") { "$it(\"$it\")" }
    }

}

